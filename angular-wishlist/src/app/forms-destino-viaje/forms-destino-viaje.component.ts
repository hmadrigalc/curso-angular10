import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

@Component({
  selector: 'app-forms-destino-viaje',
  templateUrl: './forms-destino-viaje.component.html',
  styleUrls: ['./forms-destino-viaje.component.scss']
})
export class FormsDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud= 3;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();    
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(3)
      ])],
      url: ['',Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
        console.log('cambio el formulario: ', form);      
    });
  }

  ngOnInit(){
    //seleccionamos el elemento html
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    //detecta cada vez que el usuario toca una tecla sobre la caja de texto
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent)=>(e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      //tiempo antes que se actualice la cadena de busqueda
      debounceTime(200),
      //continua si la cadena varia
      distinctUntilChanged(),
      //similar a consultar un webservice
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      this.searchResults = ajaxResponse.response;
    });
  }

  guardar(nombre: string, url: string):boolean{
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  //valor de retorno de un validador siempre es un array con valores especificos
  nombreValidator(control: FormControl): { [s: string]: boolean }{
    const l = control.value.toString().trim().length;
    if(l>0 && l< 5){
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[s:string]:boolean} | null => {
      const l = control.value.toString().trim().length;
      if(l>0 && l< minLong){
        return { minLongNombre: true };
      }
      return null;
    }
  }
}
